#!/bin/bash

docker-compose down
rm -rf main/data/pgdata/
rm -rf shard-1/data/pgdata/
rm -rf shard-2/data/pgdata/
docker-compose build
docker-compose up -d postgresql-b postgresql-b1 postgresql-b2
